module.exports = grammar({
    name: 'o2a',

    extras: $ => [$._whitespace, $.block_comment, $.pragma],

    word: $ => $.identifier,

    rules: {
        module: $ => seq(
            $.module_decl,
            $.import_section,

            repeat($.const_type_var_section),

            repeat($.procedure_definition)
        ),

        module_decl: $ => seq(
            'MODULE', $.identifier, ';'
        ),

        import_section: $ => seq(
            'IMPORT',
            $.import_decl,
            repeat(seq(',', $.import_decl)),
            ';'
        ),

        import_decl: $ => choice(
            seq($.identifier, ':=', $.identifier),
            seq($.identifier)
        ),

        const_type_var_section: $ => choice(
            $.const_section,
            $.type_section,
            $.var_section
        ),

        const_section: $ => seq(
            'CONST',
            repeat1($.const_decl)
        ),

        const_decl: $ => seq(
            $.identifier,
            '=',
            choice($.string_literal),
            ';'
        ),

        type_section: $ => seq(
            'TYPE', repeat1(seq($.type_definition, ';'))
        ),

        type_definition: $ => seq(
            $.identifier, '=', $.type_body
        ),

        type_body: $ => choice(
            seq('POINTER', optional($.find_out_name_of_oa_extension), 'TO', 'ARRAY', $.constant_integer_expression, 'OF', $.designator)
        ),

        find_out_name_of_oa_extension: $ => seq(
            '[', /[1-4]/, ']'
        ),

        var_section: $ => seq(
            'VAR',
            repeat1($.var_decl)
        ),

        var_decl: $ => seq(
            $.identifier,
            repeat(seq(
                ',', $.identifier
            )),
            ':',
            $.type_spec,
            ';'
        ),

        type_spec: $ => choice(
            $.designator,
            $.anonymous_record_definition
        ),

        anonymous_record_definition: $ => seq(
            'RECORD', optional($.find_out_name_of_oa_extension), optional($.base_record_specification),
            repeat(seq($.record_member_item, ';')),
            'END'
        ),

        base_record_specification: $ => seq(
            '(', $.designator, ')'
        ),

        record_member_item: $ => seq(
            $.identifier, repeat(seq(',', $.identifier)), ':', $.designator
        ),

        designator: $ => seq(
            $.identifier, repeat(seq('.', $.identifier))
        ),

        procedure_definition: $ => seq(
            'PROCEDURE', optional('*'), $.identifier, optional($.formal_parameter_list), ';',

            optional($.var_section),

            'BEGIN',
            choice(
                seq($.statement, optional(';')),

                seq(
                    repeat(seq($.statement, ';')),
                    $.statement, optional(';')
                ),
            ),
            'END', $.identifier, ';'
        ),

        formal_parameter_list: $ => seq(
            '(',
            choice(
                optional($.formal_parameter_list_item),

                seq($.formal_parameter_list_item, repeat1(seq(';', $.formal_parameter_list_item)))
            ),
            ')'
        ),

        formal_parameter_list_item: $ => seq(
            optional('VAR'),
            $.identifier, repeat(seq(',', $.identifier)),
            ':',
            $.type_spec
        ),

        statement: $ => choice(
            $.if_statement,
            $.assign_statement,
            $.proc_or_fun_call
        ),

        if_statement: $ => seq(
            'IF', $.boolean_expression, 'THEN',
            choice(
                $.statement,
                seq(repeat1(seq($.statement, ';')), $.statement)
            ),
            'END'
        ),

        assign_statement: $ => seq(
            $.designator, ':=', $.expression
        ),

        proc_or_fun_call: $ => seq(
            $.designator, $.argument_list
        ),

        argument_list: $ => choice(
            seq('(', ')'),

            seq('(', $.expression, ')'),

            seq('(', $.expression, repeat1(seq(',', $.expression)), ')')
        ),

        expression: $ => choice(
            seq('(', $.expression, ')'),

            $.proc_or_fun_call,

            prec.left(seq($.expression, '#', $.expression)),

            prec.left(seq($.expression, '&', $.expression)),

            $.designator
            
        ),

        constant_integer_expression: $ => choice(
            seq('MAX', '(', 'INTEGER', ')')  // TODO generalize
        ),

        boolean_expression: $ => choice(
            prec.left(seq($.expression, '#', $.expression)),

            $.designator
        ),

        _whitespace: $ => /\s/,

        block_comment: $ => seq(
            '(*',
            /([^*]|\*[^)])*/,
            '*)'
        ),

        pragma: $ => /<\*.*\*>/,

        string_literal: $ => repeat1(/".*?"/),

        identifier: $ => /[A-Za-z][A-Za-z0-9]*/,

    }
});
